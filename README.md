# Cluster and Cloud Computing 2017 Project
## Instruction

This is the front-end of this project
Author: Claude Chen
Created Date: 28 April 2017

Files Description.

| Files or Directories name | Description |
| --- | --- |
| ./index.html | The main application interface using SA2 data |
| ./index2.html | The same interface like index.html but using LGA data |
| ./css | This folder contains css stylesheets |
| ./data | This folder contains LGA and SA2 boundaries data|
| ./js | This folder contains all javascript codes used in this applicaiton |
| ./js/mapboxControl.js | The main javascript code provides functions like loading data, adding layers to the mapping platform and control the interactive events |
| .js/mapboxControl2.js | Same like mapboxCrontrol.js but assign jobs to ./index2.html
